import pygame
import time
from pygame.locals import *
from random import randrange, random
from math import *

pygame.init()

width, height = 1280, 400

window = pygame.display.set_mode((width, height))

pygame.key.set_repeat(400, 30)
pygame.mouse.set_visible(True)
clock = pygame.time.Clock()
font = pygame.font.SysFont(None, 30, bold=False)

window.fill((0,0,0))

continuer = True
imax = width
choiced = imax
factor = 0.08
enlarge = 1
latence = []
latmax = 1
take = False
color = 0
moment = 0

for i in range(0,imax+1):
    latence.append(0)
    pass
pass









while continuer == True:



    if take == True:
        take = False
        latence[imax] = pygame.time.get_ticks() - moment
        if moment == 0:
            latence[imax] = 0
        moment = pygame.time.get_ticks()
        if latmax < latence[imax]:
            latmax = latence[imax]
        for i in range(0, imax):
            latence[i] = latence[i+1]
            pass
        pass


    window.fill((0,0,0))
    for i in range(0,imax):
        color = (latence[i]/latmax)*255
        pygame.draw.line(window, (color,255-color,0), ((i), (height*3.5/4)), ((i), (height*3.5/4)-(latence[i]*factor)), 1)
        pass
    color = (latence[choiced]/latmax)*255
    pygame.draw.line(window, (color,255-color,0), ((choiced), (height*3.5/4)), ((choiced), (height*3.5/4)-(latence[choiced]*factor)), 3)
    pygame.draw.line(window, (color,255-color,0), ((choiced), (height*1.6/4)), ((choiced), (height*1.6/4)-(latence[choiced]*factor)), 3)
    text = font.render(str(choiced), True, (color,255-color,0))
    window.blit(text,(width-50,height-20))
    text = font.render('{0} ms'.format(latence[choiced]), True, (color,255-color,0))
    window.blit(text, (20,height-20))
    pygame.display.flip()
    clock.tick(60)

    for event in pygame.event.get():
        if event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                continuer = False
            if event.key == K_SPACE:
                take = True
            if event.key == K_RIGHT and choiced < imax:
                choiced += 1
            if event.key == K_LEFT  and choiced > 0:
                choiced -= 1

            if event.key == K_PLUS:
                enlarge = enlarge*1.1
            if event.key == K_MINUS:
                enlarge = enlarge*0.9
        if event.type == MOUSEBUTTONDOWN:
            if event.button == 1:
                take = True
            if event.button == 4:
                factor = factor * 1.1
            if event.button == 5:
                factor = factor * 0.9
        if event.type == QUIT:
            continuer = False
        souris_x, souris_y = pygame.mouse.get_pos()
        pass

    continue

















''' END '''
