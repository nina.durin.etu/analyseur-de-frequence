# Analyseur de Fréquence

![Capture d'écran du programme](./Capture_d_écran.png)

## Introduction

L'Analyseur de Fréquence est un outil simple qui permet d'analyser et de visualiser la fréquence d'événements en temps réel. Il peut être utile dans des domaines tels que la santé, où il peut par exemple servir à analyser le rythme cardiaque.

## Fonctionnalités

- Enregistrement d'événements en temps réel en appuyant sur la touche "Espace".
- Visualisation graphique des distances temporelles entre les événements enregistrés.
- Affichage d'un graphique coloré pour identifier facilement les variations de fréquence.
- Un curseur pour analyser chaque événement et la fréquence qui lui est associée.
- Capacité de mémoire jusqu'à 1280 événements.

## Comment ça marche

1. **Enregistrement d'événements**: Appuyez sur la touche "Espace" chaque fois que vous notez un événement. L'Analyseur de Fréquence enregistrera la marque temporelle de cet événement.

2. **Visualisation en temps réel**: Après avoir enregistré un événement, vous verrez un graphique qui affiche la distance temporelle entre les événements enregistrés. Ce graphique est coloré pour faciliter l'identification des variations de fréquence.

3. **Analyse détaillée**: Utilisez le curseur pour sélectionner un événement spécifique et voir la fréquence qui lui est associée.

4. **Mémoire**: L'Analyseur de Fréquence a une capacité de mémoire de 1280 événements. Après avoir atteint cette limite, il commencera à écraser les données les plus anciennes.


## Cas d'utilisation

Un des cas d'utilisation de l'Analyseur de Fréquence est l'analyse du rythme cardiaque. En enregistrant les battements cardiaques comme événements, l'utilisateur peut visualiser comment le rythme cardiaque change avec le temps, ce qui peut être utile pour l'analyse médicale.

## Prérequis

 - Python
 - Module Pygame pour python (`pip install pygame`)

## Installation

`pip install pygame`  
`git clone https://gitlab.univ-lille.fr/nina.durin.etu/analyseur-de-frequence.git`

## Démarrage rapide

`cd analyseur-de-frequence`  
`python3 main.py`

## Licence

`CC-0`  
Ce projet est sous licence libre CC-0, il peut donc être librement partagé, modifié, utilisé commercialement sans aucune attribution.
